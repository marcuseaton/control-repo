class profile::ssh_server {
	package {'openssh-server':
		ensure => present,
	}
	service { 'sshd':
		ensure => 'running',
		enable => 'true',
	}
	ssh_authorized_key { 'root@master.puppet.vm':
		ensure => present,
		user   => 'root',
		type   => 'ssh-rsa',
		key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDT0/YhBxSO5Vw042R6s1bcVa3/oEBjLLhunwwTAaT0ggiVOPj5lcRGe/00u4VrFhL3/BGD7/UY/ZUWoQ17xi9fJHIUsKZZJqgGSvkFmtnAQN8ljFIIv432ujDKwlJJBrUxbeCYwSV+RUeFgk8uL9Zy+XxOfqcCoufz6jdkQYFeDkuxroQ+jQl4FWHP4hd0Pd8YdPsO1OUhQ+bku/QTOz902hsn0c1n8jn4scUnapMJJ39f3yFYlbW5VBXJ3RP+DI66eH+40qU+4mypYWSPieouHfw0EXWrNqljzjLWu7VSFFmBLEBVdMQubZR2GW80fB14Mh4cZP7au7fBBHl4aYvt',
	}  
}
